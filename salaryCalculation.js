
const res=require('./response')

function calculateTax(slab,perMonthSalary){
    return Math.round(slab.constantCharge +((perMonthSalary-slab.slabStartingAmount) * slab.reoccuringCharge))
}


function getMonth(number) {
    let month = null
    switch (number) {
        case 0:
            month = 'January'
            break;
        case 1:
            month = 'February'
            break;
        case 2:
            month = 'March'
            break;
        case 3:
            month = 'April'
            break;
        case 4:
            month = 'May'
            break;
        case 5:
            month = 'June'
            break;
        case 6:
            month = 'July'
            break;
        case 7:
            month = 'August'
            break;
        case 8:
            month = 'September'
            break;
        case 9:
            month = 'October'
            break;
        case 10:
            month = 'November'
            break;
        case 11:
            month = 'December'
            break;
        default:
        // default case handled here
    }
    return month
}




function calculateSalaryDetails(request, response) {
    let section = 0
    const firstName = request.body.firstName
    const lastName = request.body.lastName
    const annualSalary = request.body.annualSalary
    const superRate = request.body.superRate
    const paymentStartDate = request.body.paymentStartDate
    const perMonthSalary = Math.round(annualSalary / 12)
    let slab={}
    let tax=0

    let backResponseObj = {}
    if (perMonthSalary >= 0 && perMonthSalary <= 18200) {
        section = 1
    }
    if (perMonthSalary >= 18201 && perMonthSalary <= 37000) {
        section = 2
    }
    if (perMonthSalary >= 37001 && perMonthSalary <= 87000) {
        section = 3
    }
    if (perMonthSalary >= 87001 && perMonthSalary <= 180000) {
        section = 4
    }
    if (perMonthSalary >= 180001) {
        section = 5
    }


    console.log('<<<<<<<<<<<<<SECTION>>>>>>>>>>>>>>',section)


    switch (section) {
        case 1:

         slab={
            constantCharge:0,
            reoccuringCharge:0,
            slabStartingAmount:0
        }
          tax=   calculateTax(slab,perMonthSalary)

            backResponseObj = {
                firstName,
                lastName,
                payPeriod:getMonth(new Date(paymentStartDate).getMonth()),
                grossIncome:perMonthSalary,
                incomeTax:tax,
                netIncome:perMonthSalary-tax,
                superAnnuation:Math.round(perMonthSalary * superRate)
            }

            res.sendSuccess({statusCode:
                200,message:'success',data:backResponseObj},response)




            break;


        case 2:

         slab={
            constantCharge:0,
            reoccuringCharge:0.19,
            slabStartingAmount:18201
        }
          tax=   calculateTax(slab,perMonthSalary)

            backResponseObj = {
                firstName,
                lastName,
                payPeriod:getMonth(new Date(paymentStartDate).getMonth()),
                grossIncome:perMonthSalary,
                incomeTax:tax,
                netIncome:perMonthSalary-tax,
                superAnnuation:Math.round(perMonthSalary * superRate)
            }

            res.sendSuccess({statusCode:
                200,message:'success',data:backResponseObj},response)

            break;



        case 3:

         slab={
            constantCharge:3572,
            reoccuringCharge:0.325,
            slabStartingAmount:37001
        }
          tax=   calculateTax(slab,perMonthSalary)

            backResponseObj = {
                firstName,
                lastName,
                payPeriod:getMonth(new Date(paymentStartDate).getMonth()),
                grossIncome:perMonthSalary,
                incomeTax:tax,
                netIncome:perMonthSalary-tax,
                superAnnuation:Math.round(perMonthSalary * superRate)
            }

            res.sendSuccess({statusCode:
                200,message:'success',data:backResponseObj},response)
            break;
        case 4:

         slab={
            constantCharge:19822,
            reoccuringCharge:0.37,
            slabStartingAmount:87001
        }
          tax=   calculateTax(slab,perMonthSalary)

            backResponseObj = {
                firstName,
                lastName,
                payPeriod:getMonth(new Date(paymentStartDate).getMonth()),
                grossIncome:perMonthSalary,
                incomeTax:tax,
                netIncome:perMonthSalary-tax,
                superAnnuation:Math.round(perMonthSalary * superRate)
            }

            res.sendSuccess({statusCode:
                200,message:'success',data:backResponseObj},response)
            break;
        case 5:

         slab={
            constantCharge:54232,
            reoccuringCharge:0.45,
            slabStartingAmount:180001
        }
          tax=   calculateTax(slab,perMonthSalary)

            backResponseObj = {
                firstName,
                lastName,
                payPeriod:getMonth(new Date(paymentStartDate).getMonth()),
                grossIncome:perMonthSalary,
                incomeTax:tax,
                netIncome:perMonthSalary-tax,
                superAnnuation:Math.round(perMonthSalary * superRate)
            }

            res.sendSuccess({statusCode:
            200,message:'success',data:backResponseObj},response)
            break;

        default:

        //default handling here
    }
}

module.exports=calculateSalaryDetails