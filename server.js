
const http = require('http')
const server = http.createServer()
const salaryCalculation=require('./salaryCalculation')
const res=require('./response')



//server object here is an event emitter
server.on('request', (request, response) => {
    let fullRequest = []
    const { method, url, headers } = request
    if (method === 'POST' && url === '/getSalaryDetails') {
        request.on('data', (chunk) => {// request here is a readable stream and instance of EventEmitter class
            fullRequest.push(chunk)

        })

        request.on('error', (error) => {
            console.log('<<<<<<<<<<<<<<AN ERROR OCCURS WHILE INCOMING REQUEST>>>>>>', error)
        })

        request.on('end', () => {
            try{
                fullRequest = JSON.parse(Buffer.concat(fullRequest).toString())//validating if server is receiving JSON or not

            }
            catch(error){
                res.sendBadRequest({
                    statusCode:400,
                    error:'Bad Request',
                    message:'Json validation failed'
                },response)
            }
           
            request.body = fullRequest // full request body now is received
            salaryCalculation(request, response)

        
        })

    }
   else if(method==='GET' && url==='/'){
        res.sendSuccess({
            message:'Hello World',
            statusCode:200,
            data:{}
        },response)
    }
    else {
        const obj={
            message:'page not found',
            statusCode:'404',
            error:'not found'
        }
       res.sendNotFound(obj,response)
    }
})

server.listen(3000,function(){
    console.log('<<<<<<<<<<<SERVER IS LISTENING ON localhost:3000 >>>>>>>>')
})
