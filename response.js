function sendSuccess(message,response){
    let msg=JSON.stringify(message)
    response.writeHead(200,{
        'Content-Type': 'application/json',
        'Content-Length':msg.length
    })
    response.end(msg)
}


function sendNotFound(message,response){
    let msg=JSON.stringify(message)
    response.writeHead(404,{
        'Content-Type': 'application/json',
        'Content-Length':msg.length
    })
    response.end(msg)
}


function sendBadRequest(message,response){
    let msg=JSON.stringify(message)
    response.writeHead(400,{
        'Content-Type': 'application/json',
        'Content-Length':msg.length
    })
    response.end(msg)
}

module.exports={
    sendSuccess,
    sendNotFound,
    sendBadRequest
}

